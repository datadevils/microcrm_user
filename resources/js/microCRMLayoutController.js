// Controller for changing complete layout
microCRMUserApp.controller("LayoutController", function ($scope, $rootScope, $http, Authentication, $upload) {

    // init database and broadcast event for success
    $http({
        method: 'PUT',
        url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/initdatabase',
        //data: {'name': $scope.currentcompany.name},
        headers: {'Content-Type': 'application/json'}

    }).success(function () {
        console.log("database initiated");
    }).error(function () {
        console.log("Cannot init databse");
    });


    $rootScope.layouts = [{"path": "views/home.html"},
        {"path": "views/login.html"},
        {"path": "views/newUser.html"}];

    $scope.user = {
        id: null,
        email: null,
        name: null,
        password: null};

    $rootScope.loginUser = {
        id: null,
        email: null,
        name: null,
        password: null};

    $scope.selectedFile = "";
    $scope.model = {};
    $rootScope.selectedLayout = $rootScope.layouts[1];
    $rootScope.cookieExists = false;

    $scope.login = function () {
        // $rootScope.loggedInUserName = $scope.user.name;

        if ($rootScope.cookieExists === false) {
            Authentication.ClearCredentials();
            Authentication.SetCredentials($scope.user.username,
                    $scope.user.password);
            $rootScope.cookieExists = true;

        }
        $http({
            method: 'POST',
            url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/user',
            data: {'email': $rootScope.credentials.currentUser.username},
            headers: {'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'}
        }).success(function (data) {
            $rootScope.loginUser = data;
            $scope.loginUser = data;
            console.log($scope.user.name);
            console.log("Login User: " + data);
            $rootScope.selectedLayout = $rootScope.layouts[0];
            $rootScope.$broadcast('initReady');
        }).error(function () {
            console.log("Cannot get User data");
            $rootScope.selectedLayout = $rootScope.layouts[1];
            Authentication.ClearCredentials();
            $rootScope.cookieExists = false;
            $scope.loginScreenMessage = "Invalid Username or Password!";
        });
    };

    $scope.onFileSelect = function ($files) {
        $scope.uploadProgress = 0;
        $scope.selectedFile = $files;
    };

    $scope.uploadFile = function () {
        var file = $scope.selectedFile[0];
        $scope.upload = $upload.upload({
            url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/upload',
            method: 'POST',
            fields: {'description': $scope.user.email},
            //   description: angular.toJson($scope.user.email),
            /*	data: {'email': $scope.user.email,
             'name': $scope.user.name,
             'password': $scope.user.password }, */
            file: file
        }).success(function (data) {
            console.log("user picture uploaded");
        });
    };


    $scope.signup = function () {
        console.log("posting new user");
        console.log($scope.newUser);
        console.log($scope.verify);

        $http({
            method: 'POST',
            url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/addNewUser',
            data: {'email': $scope.user.email,
                'name': $scope.user.name,
                'password': $scope.user.password},
            headers: {'Content-Type': 'application/json'}
        }).success(function () {
            console.log("newUser posted successfuly!");
            $scope.loginScreenMessage = "Signup succesful!";
            if ($scope.selectedFile !== "") {
                $scope.uploadFile();
            }
            $scope.user.name = "";
            $scope.user.password = "";
            $rootScope.selectedLayout = $rootScope.layouts[1];
        }).error(function () {
            console.log("failed to add newUser");
            $scope.loginScreenMessage = "Signup failed...";
        });
    };

    $scope.signupPage = function () {
        $rootScope.selectedLayout = $rootScope.layouts[2];
        $scope.user.name = "";
        $scope.user.password = "";
        $scope.verify = "";
        $scope.user.email = "";
        $scope.newUserDisabled = true;
    };

    $scope.return = function () {
        $scope.user.name = "";
        $scope.user.password = "";
        $scope.verify = "";
        $scope.user.email = "";
        $rootScope.selectedLayout = $rootScope.layouts[1];
    };

    if (Authentication.GetCredentials()) {
        $scope.username = $rootScope.credentials.currentUser.username;
        $rootScope.cookieExists = true;
        $scope.login();
    }
});

