// Controller for getting company info
microCRMUserApp.controller("CompaniesCtrl", function ($scope, $rootScope, $http, $filter) {

    $scope.companies = [];

    // init database and broadcast event for success
    $http({
        method: 'PUT',
        url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/initdatabase',
        //data: {'name': $scope.currentcompany.name},
        headers: {'Content-Type': 'application/json'}

    }).success(function () {
        console.log("database initiated");
        $scope.sendInitReady();

    }).error(function () {
        console.log("Cannot init databse");
    });

    $scope.sendInitReady = function () {
        $scope.companies = null;
        $rootScope.$broadcast('initReady');
    };

    $scope.visibleCompanies = [10];

    $scope.selectedIndex = -1;

    // Variable to keep count on what page is being shown
    $scope.selectedPage = 0;

    // Filter for sorting company data alphabetically by predicate
    var orderBy = $filter('orderBy');

    $scope.order = function (predicate, reverse) {
        $scope.companies = orderBy($scope.companies, predicate, reverse);
    };

    // Function for updating one page or showing the firts page
    // when application starts
    $scope.viewFirstCompanyPage = function () {
        $scope.order('-name', true);
        $scope.visibleCompanies = [];
        var maxPages = Math.ceil(($scope.companies.length / 10));
        if ($scope.selectedPage === 0) {
            $scope.previousCompanyDisabled = true;
        }
        var max;
        var start = ($scope.selectedPage * 10);
        if ($scope.selectedPage === (maxPages - 1)) {
            max = ($scope.companies.length - start);
            $scope.nextCompanyDisabled = true;
        } else {
            max = 10;
            $scope.nextCompanyDisabled = false;
        }
        for (var i = 0; i < max; i++) {
            $scope.visibleCompanies[i] = $scope.companies[(i + start)].valueOf();
        }
    };

    // Function for showing next company page
    $scope.nextCompanyPage = function () {
        $scope.previousCompanyDisabled = false;
        $scope.selectedPage++;
        $scope.visibleCompanies = [];
        var maxPages = Math.ceil(($scope.companies.length / 10));
        var start = ($scope.selectedPage * 10);
        var max;
        if ($scope.selectedPage === (maxPages - 1)) {
            max = ($scope.companies.length - start);
            $scope.nextCompanyDisabled = true;
            $scope.nextCompanyDisabled = true;
        } else {
            max = 10;
            $scope.nextCompanyDisabled = false;
        }
        for (var i = 0; i < max; i++) {
            $scope.visibleCompanies[i] = $scope.companies[(i + start)].valueOf();
        }
    };

    // Function for showing previous company page
    $scope.previousCompanyPage = function () {
        $scope.nextCompanyDisabled = false;
        $scope.selectedPage--;
        $scope.visibleCompanies = [];
        if ($scope.selectedPage === 0) {
            $scope.previousCompanyDisabled = true;
        }
        else {
            $scope.previousCompanyDisabled = false;
        }
        var start = (($scope.selectedPage) * 10);
        for (var i = 9; i >= 0; i--) {
            $scope.visibleCompanies[i] = $scope.companies[(i + start)].valueOf();
        }
    };

    $rootScope.$on('initGetPersons', function (event) {
        $scope.dummyCompany = {
            id: null,
            name: null,
            logo_id: null
        };
        $scope.getPersons($scope.dummyCompany, -1);
    });

    // broadcast event to get person data
    $scope.getPersons = function (company, $index) {

        $scope.selectedIndex = $index;
        $rootScope.$broadcast('transmitSelectedPerson', null);
        $rootScope.$broadcast('transmitSelectedCompany', company);
        $scope.currentcompany = company;

        console.log("company id" + $scope.currentcompany.id);
        console.log("company name" + $scope.currentcompany.name);
        console.log("company logo_id" + $scope.currentcompany.logo_id);


        $rootScope.$broadcast('personsRequest', company);
    };

    // when init is ready this is called to get company data
    $scope.$on('initReady', function (event) {
        $http({
            method: 'GET',
            url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/listcompanies',
            headers: {'Content-Type': 'application/json'}
        }).success(function (data) {
            $scope.companies = data;
            $rootScope.companies = data;
            if ($scope.companies) {
                $scope.viewFirstCompanyPage();
                console.log("company page updated");
            }
            console.log("Company data fetched");
        }).error(function (data, status) {
            console.log("company data in error" + data);
            console.log("get company status" + status);
            console.log("Cannot get company data");
        });
    });

    $scope.newCompany = function () {
        console.log("Clicked New Company button!");
        $scope.viewNewCompany();
    };

    $rootScope.$broadcast('initReady');
});
