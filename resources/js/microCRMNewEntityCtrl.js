// Controller for adding new Entities
microCRMUserApp.controller("NewEntityCtrl", function ($scope, $rootScope, $http, $upload) {

    // Lets format the necessary lists for new entities
    $scope.newCompany = {
        name: null
    };

    $scope.newPerson = {
        name: null,
        title: null,
        email: null,
        phone: null,
        company: null
    };

    $scope.newTask = {
        description: null,
        start_date: null,
        due_date: null,
        company: null,
        person: null
    };

    $scope.newFollowupTask = {
        description: null,
        start_date: null,
        due_date: null,
        company: null,
        person: null,
        contact: null
    };

    $scope.newEntityMessage = "";

    $scope.selectedFile = "";
	
    $scope.onFileSelect = function ($files) {
        $scope.uploadProgress = 0;
        $scope.selectedFile = $files;
    };

    $scope.uploadFile = function () {
        var file = $scope.selectedFile[0];
        $scope.upload = $upload.upload({
            url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/upload',
            method: 'POST',
            fields: {'description': $scope.newPerson.email},
            file: file
        }).success(function (data) {
            console.log("person picture uploaded");
        });
    };	
	
    // Gets company list trough rootscope
    //$scope.companyList = $rootScope.companies;

    // Lets read company and person from rootScope
    // person and company have value if they are already selected in UI
    // otherwise they will write null
    // This type of writing is used to receive selected companies and persons
    // when view for new entity is not yet open
    $scope.newPerson.company = $rootScope.selectedCompany;
    $scope.newTask.person = $rootScope.selectedPerson;
    $scope.newTask.company = $rootScope.selectedCompany;
    $scope.newFollowupTask.company = $rootScope.selectedCompany;
    $scope.newFollowupTask.person = $rootScope.selectedPerson;
    $scope.newFollowupTask.contact = $rootScope.contactForNewFollowupTask;

    //Function to receive clicked person if new entity view is already open
    $scope.$on('transmitSelectedPerson', function (event, person) {
        $scope.newFollowupTask.person = person;
        $scope.newTask.person = person;
    });

    //Function to receive clicked company if new entity view is already open
    $scope.$on('transmitSelectedCompany', function (event, company) {
        $scope.newFollowupTask.company = company;
        $scope.newTask.company = company;
        $scope.newPerson.company = company;
    });

    // This function writes all new entity companies and persons as null
    // Used for clear button
    $scope.clearCompanyAndPerson = function () {
        $scope.newPerson.company = null;
        $scope.newTask.company = null;
        $scope.newFollowupTask.company = null;
        $scope.newTask.person = null;
        $scope.newFollowupTask.person = null;
        $rootScope.selectedCompany = null;
        $rootScope.selectedPerson = null;
        //$rootScope.$broadcast('personsRequest', null);
        $rootScope.$broadcast('initGetPersons');
    };

    // Gets persons for selected company if selected company is not null
    /*$scope.getPersonsForTaskCompany = function () {
     console.log("Fetching Persons for Company");
     console.log($scope.newTask.company);
     if ($scope.newTask.company !== null) {
     $http({
     method: 'POST',
     url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/personsbycompany',
     data: {'id': $scope.newTask.company.id,
     'name': $scope.newTask.company.name,
     'logo_id': $scope.newTask.company.logo_id},
     headers: {'Content-Type': 'application/json'}
     }).success(function (data) {
     $scope.personList = data;
     console.log("data fetch success");
     }).error(function () {
     console.log("Cannot get persons data");
     });
     }
     ;
     };*/

    // Submits new task form
    $scope.submitTaskForm = function () {
        console.log("posting data...");
        console.log($scope.newTask);
        $http({
            method: 'POST',
            url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/addNewTask',
            data: {'description': $scope.newTask.description,
                'start_date': $scope.newTask.start_date,
                'due_date': $scope.newTask.due_date,
                'company': $scope.newTask.company,
                'person': $scope.newTask.person},
            headers: {'Content-Type': 'application/json'}
        }).success(function () {
            console.log("Data posted succesfuly!");
            // Broadscast initready to refresh tables with new data
            $rootScope.$broadcast('initReady');
            $scope.newEntityMessage = "New task added succesfuly!";
        }).error(function () {
            console.log("Could not set new task data");
            $scope.newEntityMessage = "Failed to add new task...";
        });
    };

    // submits new company form
    $scope.submitCompanyForm = function () {
        console.log("posting data...");
        console.log($scope.newCompany);
        $http({
            method: 'POST',
            url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/addNewCompany',
            data: {'name': $scope.newCompany.name},
            headers: {'Content-Type': 'application/json'}
        }).success(function () {
            console.log("Data posted succesfuly!");
            $scope.newEntityMessage = "New Company added succesfuly!";
            // Broadscast initready to refresh tables with new data
            $rootScope.$broadcast('initReady');
        }).error(function () {
            console.log("Could not set new task data");
            $scope.newEntityMessage = "Failed to add new Company...";
        });
    };

    // submits new person form
    $scope.submitPersonForm = function () {
        console.log("posting data...");
        console.log($scope.newPerson);
        $http({
            method: 'POST',
            url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/addNewPerson',
            data: {'name': $scope.newPerson.name,
                'title': $scope.newPerson.title,
                'email': $scope.newPerson.email,
                'phone': $scope.newPerson.phone,
                'company': $scope.newPerson.company},
            headers: {'Content-Type': 'application/json'}
        }).success(function (data) {
            console.log("Data posted succesfuly!");
			if ($scope.selectedFile !== "") {
                $scope.uploadFile();
            }
            $scope.newEntityMessage = "New Person added succesfuly!";
            // Broadscast initready to refresh tables with new data
            $rootScope.$broadcast('initReady');
            // Broadcast personsRequest with returned company to refresh
            // personlist for selected company
            $rootScope.$broadcast('personsRequest', data.company);

        }).error(function () {
            console.log("Could not set new task data");
            $scope.newEntityMessage = "Failed to add new Person...";
        });
    };

    $scope.submitFollowupTaskForm = function () {
        console.log("posting data...");
        console.log($scope.newTask);
        $http({
            method: 'POST',
            url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/addNewFollowupTask',
            data: {
                'description': $scope.newFollowupTask.description,
                'start_date': $scope.newFollowupTask.start_date,
                'due_date': $scope.newFollowupTask.due_date,
                'company': $scope.newFollowupTask.company,
                'person': $scope.newFollowupTask.person,
                'contact': $scope.newFollowupTask.contact
            },
            headers: {'Content-Type': 'application/json'}
        }).success(function (data) {
            console.log("Data posted succesfuly!");
            // Broadscast initready to refresh tables with new data
            $rootScope.$broadcast('initReady');
            $rootScope.$broadcast('contactRequest', data);
            $scope.viewContacts();
        }).error(function () {
            console.log("Could not set new task data");
            $scope.newEntityMessage = "Failed to add new follow up task...";
        });
    };
});


