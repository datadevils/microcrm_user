/* 
 Created on : Feb 19, 2015, 3:38:15 PM
 Author     : mika
 Handles data list operation from rest client
 * */

var microCRMUserApp = angular.module('microCRMUserApp', ['ngCookies', 'angularFileUpload']);

// Filter for modifying dates in the view        
microCRMUserApp.filter('dateFormat', function ($filter) {
    return function (input)
    {
        /*
        if (input === null) {
            return "";
        }
        */
       if (!input) {
            return "";
        }

        var _date = $filter('date')(new Date(input), 'MMM dd yyyy');
        return _date.toUpperCase();
    };
});

//$scope.dateToShow = $filter('date')(dateObj,'yyyy-MM-dd');
// console.log($scope.dateToShow);

/*
 $scope.companies = [
 {"name": "Patria", "address": "Naulakatu"},
 {"name": "Rovio", "address": "Finlayson"},
 {"name": "Opiframe", "address": "Oulu"},
 {"name": "Wapice", "address": "Hervanta"},
 {"name": "Viewport", "address": "J?rvikatu"}
 ];
 
 
 
 microCRMUserApp.controller("PersonsCtrl", function ($scope) {
 $scope.persons = [
 {"name": "Samuli Ahonen", "phone": "123456", "email": "sahonen@kolumbus.fi"},
 {"name": "Verneri Keskinen", "phone": "135246", "email": "vernerikeskinen@gmail.com"},
 {"name": "Mika Pihlamo", "phone": "456123", "email": "mika.pihlamo@iki.fi"},
 {"name": "Jarkko Viinikka", "phone": "654321", "email": "jarkko.t.viinikka@student.tut.fi"}
 ];
 });
 
 
 microCRMUserApp.controller("TasksCtrl", function ($scope) {
 $scope.tasks = [
 {"name": "P?ivit? CV", "due": "11.1.2015"},
 {"name": "Soita harjoittelupaikkaa", "due": "8.3.2015"},
 {"name": "Tee sopimus", "due": "21.3.2015"}
 ];
 });
 */