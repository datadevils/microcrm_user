// Controller for changing small areas in the layout
microCRMUserApp.controller("ViewController", function ($scope, $rootScope, $http, Authentication) {

    $scope.userName = $rootScope.credentials.currentUser.username;
    $scope.name = $rootScope.loginUser[0].name;

    $scope.views = [{name: 'contacts_empty', url: 'views/contacts_empty.html'},
        {name: 'contacts_full', url: 'views/contacts_full.html'},
        {name: 'newTask', url: 'views/newTask.html'},
        {name: 'newCompany', url: 'views/newCompany.html'},
        {name: 'newPerson', url: 'views/newPerson.html'},
        {name: 'newFollowupTask', url: 'views/newFollowupTask.html'},
        {name: 'newContact', url: 'views/newContact.html' }];

    $scope.view = $scope.views[0];
    $scope.email = $rootScope.credentials.currentUser.username;
    console.log("loginUser: " + $rootScope.loginUser);

    $scope.viewContacts = function () {
        $scope.view = $scope.views[1];
        console.log("view changed");
    };

    $scope.viewNewTask = function () {
        $scope.view = $scope.views[2];
        console.log("view changed");
    };

    $scope.viewNewCompany = function () {
        $scope.view = $scope.views[3];
        console.log("view changed");
    };

    $scope.viewNewPerson = function () {
        $scope.view = $scope.views[4];
        console.log("view changed");
    };

    $scope.viewNewFollowupTask = function () {
        $scope.view = $scope.views[5];
        console.log("view changed");
    };
    
    $scope.viewNewContact = function() {
        $scope.view = $scope.views[6];
        console.log("view changed");
    };

    $scope.logout = function () {
        Authentication.ClearCredentials();
        $rootScope.cookieExists = false;
        $rootScope.selectedLayout = $rootScope.layouts[1];
    };
});