// controller for getting contact info
microCRMUserApp.controller("ContactsCtrl", function ($scope, $rootScope, $http, $filter) {
    var person = {
        name: "No contact"
    };
    var no_contact = {
        person: person,
        description: ' ',
        date: null
    };
    var company = {
        name: " "
    };
    var no_task = {
        description: 'No task'
    };
    /*
     $scope.contact;
     $scope.task;
     */
    
    $scope.$on('contactRequest', function (event, task) {
       
        console.log("contactRequest");
        $scope.next_disabled = false;
        $scope.previous_disabled = false;
        $scope.task = task;
        if ($scope.task.done) {
            $scope.task.state = "finished";
        } else {
            $scope.task.state = "unfinished";
        }
        
        $scope.followupcontact = null;
        
        // Get visible contact from database
        $scope.getFollowUpContact(task);
        
    });
    
    // When followupcontact changes this is called
    $scope.$on('contactDataFetched', function () {
        if ($scope.followupcontact) {
            console.log("fisrt contact found");
            
            // set visible contact to followup contact of the visible task
            $scope.contact = $scope.followupcontact;
            console.log("visible contact: " + $scope.contact);
           
            // Get follow up task and if succeed get also follow up contact
            $scope.getFollowUpTask($scope.contact);
        } else {
            console.log("no first contact found: ");
            $scope.contact = no_contact;
            $scope.next_disabled = true;
        }

        // set previous contact and previous task
        $scope.previous_contact = $scope.task.previous;
        if ($scope.previous_contact) {
            console.log("previous contact found");
            $scope.previous_task = $scope.previous_contact.previous;
        } else {
            console.log("no previous contact found");
            $scope.previous_disabled = true;
        }

    });

// get followup contact by a task
    $scope.getFollowUpContact = function (task) {
        console.log("followUp contact Request");
        $scope.currenttask = task;
        console.log("task id" + $scope.currenttask.id);
        $http({
            method: 'POST',
            url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/followupbytask',
            data: {'id': $scope.currenttask.id, 'basket': $scope.currenttask.basket, 'person': $scope.currenttask.person,
                'company': $scope.currenttask.company, 'startDate': $scope.currenttask.startDate,
                'dueDate': $scope.currenttask.dueDate, 'description': $scope.currenttask.description,
                'done': $scope.currenttask.done, 'followUp': $scope.currenttask.followUp,
                'previous': $scope.currenttask.previous},
            headers: {'Content-Type': 'application/json'}
        }).success(function (data) {
            console.log("data fetch success");
            console.log("followup_contact: " + data);
            $scope.followupcontact = data;
            if($scope.task === task){
                $scope.$broadcast('contactDataFetched');
            }
        }).error(function (data, status) {
            $scope.followupcontact = null;
            $scope.next_disabled = true;
            console.log("followup contact data" + data);
            console.log("followup contact status" + status);
            console.log("Cannot get contacts data");
        });
    };


// get followup task by contact  and if succeed get next followup contact
    $scope.getFollowUpTask = function (contact) {
        console.log("followUp task Request");
        $scope.currentcontact = contact;

        $http({
            method: 'POST',
            url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/followupbycontact',
            data: {'id': $scope.currentcontact.id, 'basket': $scope.currentcontact.basket, 'person': $scope.currentcontact.person,
                'date': $scope.currentcontact.date, 'contactType': $scope.currentcontact.contactType,
                'description': $scope.currentcontact.description,
                'followUp': $scope.currentcontact.followUp,
                'previous': $scope.currentcontact.previous},
            headers: {'Content-Type': 'application/json'}
        }).success(function (data) {
            console.log("data fetch success");
            console.log("followup_task: " + data);
            $scope.followuptask = data;
            
            if (data) {
                $scope.getFollowUpContact($scope.followuptask);
            } else {
                $scope.followupcontact = null;
                $scope.followuptask = null;
                $scope.next_disabled = true;
            }

        }).error(function (data, status) {
            $scope.followuptask = null;
             $scope.followupcontact = null;
             $scope.next_disabled = true;
            console.log("followup task data" + data);
            console.log("followup task status" + status);
            console.log("Cannot get task data");
        });
    };

// this is called when next or "plus" button is pressed
// sets new values for $scope.task and $scope.contact and
// updates followup_task and followup_contact and also previous_task
// and previous_contact variables
    $scope.viewNextTask = function () {

        $scope.previous_disabled = false;

        // To get back form next view these have to be set
        if ($scope.task) {
            $scope.previous_task = $scope.task;
        }

        if ($scope.contact) {
            $scope.previous_contact = $scope.contact;
        }

        console.log("next called");
        // If followup task exists then set followuptask to visible task 
        if ($scope.followuptask) {
            console.log("followup task found");
            $scope.task = $scope.followuptask;
            
            // set visible state variable for task
            if ($scope.task.done) {
                    $scope.task.state = "finished";
                } else {
                    $scope.task.state = "unfinished";
                }
               
            // If followup task was found then check,
            // if there is a followupcontact
            if ($scope.followupcontact) {
                console.log("followup contact found");
                
                // If followup contact was found then set followupcontact
                //  to visible contact
                $scope.contact = $scope.followupcontact;
                
                // Get next followuptask and if this succeeds then
                // get also followup contact
                $scope.getFollowUpTask($scope.contact);
            } else {
                // If no followupcontact was found display no_contact
                // and disable next button.
                $scope.contact = no_contact;
                $scope.next_disabled = true;
            }

        } else {
            // If no followup task was found then set
            // visible contact to no_contact and visible task to no_task
            // and disable next button
            $scope.contact = no_contact;
            $scope.task = no_task;
            $scope.next_disabled = true;
        }
    };

// This is called when previous or "minus" button is pressed.
// Updates $scope.task and $scope.contact variables and
// updates previous_task and previous_contact and followup_task
// and followup_contact variables
    $scope.viewPreviousTask = function () {

        $scope.next_disabled = false;

        // To get back from previous view these have to be set
        if ($scope.task) {
            $scope.followuptask = $scope.task;
        }

        if ($scope.contact) {
            $scope.followupcontact = $scope.contact;
        }
        
        // If previous_contact exists then set 
        // previous_contact to visible contact
        console.log("previous called");
        if ($scope.previous_contact) {
            $scope.contact = $scope.previous_contact;
            console.log("previous contact found");

            // If previous_task exists then set previous_task to visible task
            if ($scope.previous_task) {
                console.log("previous contact found");
                $scope.task = $scope.previous_task;

                // set new value to previous_contact
                $scope.previous_contact = $scope.task.previous;
                console.log("previous_contact: " + $scope.previous_contact);
                
                // If new previous_contact exists then set 
                // new value to previous_task
                if ($scope.previous_contact) {
                    $scope.previous_task = $scope.previous_contact.previous;
                } else {
                    // if no new previous_contact was found
                    // then disable previous button
                    // and set previous_contact to no_contact
                    // and previous_task to no_task
                    $scope.previous_disabled = true;
                    $scope.previous_contact = null;
                    $scope.previous_task = null;
                }
                
                // set visible task state
                if ($scope.task.done) {
                    $scope.task.state = "finished";
                } else {
                    $scope.task.state = "unfinished";
                }
                
            } else {
                // If no previous task was found then disable previous button
                // and set visible task to no_task
                $scope.task = no_task;
                $scope.previous_disabled = true;
            }
        } else {
            // If no previous_contact was found then set visible contact
            // to no_contact and visible task to no_task and disable previous
            // button.
            $scope.contact = no_contact;
            $scope.task = no_task;
            $scope.previous_disabled = true;
        }
    };

    // Checks if currently viewed task has a contact
    // if true writes contact to rootscope for new follow up task creation
    // else returns false and element for adding contact is shown
    $scope.checkForContact = function () {
        if ($scope.contact) {
            if ($scope.contact.description !== " ") {
                //console.log($scope.contact);
                $rootScope.contactForNewFollowupTask = $scope.contact;
                return true;
            }
        }
        return false;
    };

    // Formats variables used for creating a new contact
    $scope.newContact = {
        person: null,
        date: null,
        description: null,
        taskdone: false,
        task: null
    };
    
    $scope.newEntityMessage = "";

    // Writes newContact.person as null and is used for clear button
    $scope.clearPerson = function () {
        $scope.newContact.person = null;
        $rootScope.selectedPerson = null;
        $rootScope.$broadcast('initGetPersons');
    };

    // Read selected person from rootscope
    // writes null if no person is selected
    // This type of writing is used to receive selected person if person is
    // selected before viewing form for new contact
    $scope.newContact.person = $rootScope.selectedPerson;

    // Function to receive person for contact when new contact from is already open
    $scope.$on('transmitSelectedPerson', function (event, person) {
        $scope.newContact.person = person;
    });

    // Submits new contact form
    $scope.submitContactForm = function () {
        $scope.newContact.task = $scope.task;
        //$scope.newContact.person = $rootScope.selectedPerson;
        console.log($scope.newContact.task);
        console.log($scope.newContact.taskdone);

        $http({
            method: 'POST',
            url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/addNewContact',
            data: {
                'person': $scope.newContact.person,
                'date': $scope.newContact.date,
                'description': $scope.newContact.description,
                'taskdone': $scope.newContact.taskdone,
                'task': $scope.newContact.task
            },
            headers: {'Content-Type': 'application/json'}
        }).success(function (data) {
            console.log("Data posted succesfuly!");
            // Broadcast initready to refresh tables
            $rootScope.$broadcast('initReady');
            // broadcast contactRequest with received data to refresh currently viewed task and contact
            $rootScope.$broadcast('contactRequest', data);
            $scope.viewContacts();
        }).error(function () {
            console.log("Could not set new contact data");
            $scope.newEntityMessage = "Adding new contact to task failed...";
        });
    };

});