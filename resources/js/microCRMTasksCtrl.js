// controller for getting task info
microCRMUserApp.controller("TasksCtrl", function ($scope, $rootScope, $http, $filter) {

    $scope.tasks = [];

    $scope.visibleTasks = [];

    $scope.previousTaskDisabled = true;
    $scope.nextTaskDisabled = true;
    // Variable to keep count on what page is being shown
    $scope.selectedPage = 0;

    // gets tasks data when init is ready
    /*
    $scope.$on('initReady', function (event) {
        $http({
            method: 'GET',
            url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/listtasks',
            headers: {'Content-Type': 'application/json'}
        }).success(function (data) {
            $scope.tasks = data;

            if ($scope.tasks) {
                $scope.viewFirstTaskPage();
                console.log("task page updated");
            }
            
            console.log("task data fetch succes");
        }).error(function (data, status) {
            console.log("task data: " + data);
            console.log("task status" + status);
            console.log("Cannot get task data");
        });
    });
    */
   
   // Get basket related to logged in user
   $scope.$on('initReady', function (event) {
       $scope.user = $rootScope.loginUser[0];
       $scope.basket = null;
       $http({
            
            method: 'POST',
            url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/basketbyuser',
            data: {'id': $scope.user.id, 'name': $scope.user.name, 'email': $scope.user.email},
            headers: {'Content-Type': 'application/json'}
        }).success(function (data) {
            $scope.basket = data;
            if($scope.basket){
                $scope.getTasksByBasket(data);
            }
            console.log("basket data fetch success");
        }).error(function (data, status) {
            console.log("basket data" + data);
            console.log("basket request status" + status);
            console.log("Cannot get basket data");
        });

    });
    
    // get tasks related to a basket the user is in
    $scope.getTasksByBasket = function(basket){
        $http({
            method: 'POST',
            url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/tasksbybasket',
            data: {'id': basket.id, 'name': basket.name, 'owner': basket.owner},
            headers: {'Content-Type': 'application/json'}
        }).success(function (data) {
            $scope.tasks = data;

            if ($scope.tasks) {
                // This filter is for filtering out allready finished tasks
                // from the task list.
                $scope.tasks = $filter('filter')($scope.tasks, {done: '!true'});
                $scope.viewFirstTaskPage();
                console.log("task page updated");
            }
            
            console.log("task data fetch succes");
        }).error(function (data, status) {
            console.log("task data: " + data);
            console.log("task status" + status);
            console.log("Cannot get task data");
        });
    };
   
    // Filter for sorting person data alphabetically by predicate
    var orderBy = $filter('orderBy');

    $scope.order = function (predicate, reverse) {
        $scope.tasks = orderBy($scope.tasks, predicate, reverse);
    };

    // Function for updating one page or showing the firts page
    // when application starts
    $scope.viewFirstTaskPage = function () {
        $scope.visibleTasks = [5];
        $scope.order('-startDate', true);
        $scope.visibleTasks = [];
        var maxPages;
        if ($scope.tasks.length > 0) {
            maxPages = Math.ceil(($scope.tasks.length / 5));
        } else {
            maxPages = 1;
        }
        console.log("max pages" + maxPages);
        if (maxPages === 1) {
            $scope.previousTaskDisabled = true;
            $scope.nextTaskDisabled = true;
        }
        if ($scope.selectedPage === 0) {
            $scope.previousTaskDisabled = true;
        }
        var max;
        var start = ($scope.selectedPage * 5);
        if ($scope.selectedPage === (maxPages - 1)) {
            max = ($scope.tasks.length - start);
            $scope.nextTaskDisabled = true;
        } else {
            max = 5;
            $scope.nextTaskDisabled = false;
        }
        for (var i = 0; i < max; i++) {
            $scope.visibleTasks[i] = $scope.tasks[(i + start)].valueOf();
        }
    };

    // Function for showing next person page
    $scope.nextTaskPage = function () {
        $scope.previousTaskDisabled = false;
        $scope.selectedPage++;
        $scope.visibleTasks = [];
        var maxPages = Math.ceil(($scope.tasks.length / 5));
        var start = ($scope.selectedPage * 5);
        var max;
        if ($scope.selectedPage === (maxPages - 1)) {
            max = ($scope.tasks.length - start);
            $scope.nextTaskDisabled = true;
            $scope.nextTaskDisabled = true;
        } else {
            max = 5;
            $scope.nextTaskDisabled = false;
        }
        for (var i = 0; i < max; i++) {
            $scope.visibleTasks[i] = $scope.tasks[(i + start)].valueOf();
        }
    };

    // Function for showing previous person page
    $scope.previousTaskPage = function () {
        $scope.nextTaskDisabled = false;
        $scope.selectedPage--;
        $scope.visibleTasks = [];
        if ($scope.selectedPage === 0) {
            $scope.previousTaskDisabled = true;
        }
        else {
            $scope.previousTaskDisabled = false;
        }
        var start = (($scope.selectedPage) * 5);
        for (var i = 4; i >= 0; i--) {
            $scope.visibleTasks[i] = $scope.tasks[(i + start)].valueOf();
        }
    };


    // this sends request for contact related to task 
    $scope.getContact = function (task) {
        $rootScope.$broadcast('contactRequest', task);
        $scope.viewContacts();
    };

    // Opens new task view
    $scope.newTask = function () {
        console.log("Clicked New Task button!");
        $scope.viewNewTask();
    };

});


