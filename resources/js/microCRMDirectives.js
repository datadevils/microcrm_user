/* Directives */
// angular.module('microCRMDirectives', [])

microCRMUserApp.directive('passwordCheck', [function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
			
			elem.on('keyup', function () {
                scope.$apply(function () {					
                    ctrl.$setValidity('pwmatch', scope.verify === scope.user.password);
                });
            });
        }
    }
}]);