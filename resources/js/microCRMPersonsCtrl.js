// Controller for getting person info
microCRMUserApp.controller("PersonsCtrl", function ($scope, $rootScope, $http, $filter) {

    $scope.persons = [];

    $scope.visiblePersons = [];

    $scope.previousPersonDisabled = true;
    $scope.nextPersonDisabled = true;
    // Variable to keep count on what page is being shown
    $scope.selectedPage = 0;

    $scope.currentcompany = null;

    $scope.$on('transmitSelectedCompany', function (event, company) {
        $scope.currentcompany = company;
        $scope.selectedPage = 0;
    });

    // this gets person data from REST when requested
    $scope.$on('personsRequest', function (event, company) {
        $scope.selectedIndex = -1;
        console.log("personsRequest");


        console.log("company id" + company.id);
        console.log("company name" + company.name);
        console.log("company logo_id" + company.logo_id);


        //$scope.currentcompany = company;

        // Write selected company to rootscope for functions that need it
        // and write selected person as null to avoid conflicts where you have
        // selected persons and companies that are not linked
        $rootScope.selectedCompany = company;
        $rootScope.selectedPerson = null;

        $http({
            method: 'POST',
            url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/personsbycompany',
            data: {'id': company.id, 'name': company.name, 'logo_id': company.logo_id},
            headers: {'Content-Type': 'application/json'}
        }).success(function (data) {
            $scope.persons = data;
            $rootScope.persons = data;


            if ($scope.currentcompany.id === company.id) {
                if ($scope.persons) {
                    $scope.viewFirstPersonPage();
                    console.log("person page updated");
                }
            }

            console.log("data fect success");
        }).error(function (data, status) {
            console.log("person data" + data);
            console.log("person request status" + status);
            console.log("Cannot get persons data");
        });

        $scope.selectedIndex = -1;

        $scope.personClicked = function ($index, person) {
            console.log(person.name);
            $scope.selectedIndex = $index;
            // Broadcasts selected person for functions that need it
            $rootScope.$broadcast('transmitSelectedPerson', person);
            $rootScope.selectedPerson = person;
        };

    });

    $scope.newPerson = function () {
        console.log("Clicked New Person button!");
        $scope.viewNewPerson();
    };

    // Filter for sorting person data alphabetically by predicate
    var orderBy = $filter('orderBy');

    $scope.order = function (predicate, reverse) {
        $scope.persons = orderBy($scope.persons, predicate, reverse);
    };

    // Function for updating one page or showing the firts page
    // when application starts
    $scope.viewFirstPersonPage = function () {
        $scope.visiblePersons = [5];
        $scope.order('-name', true);
        $scope.visiblePersons = [];
        var maxPages;
        if ($scope.persons.length > 0) {
            maxPages = Math.ceil(($scope.persons.length / 5));
        } else {
            maxPages = 1;
        }
        console.log("max pages" + maxPages);
        if (maxPages === 1) {
            $scope.previousPersonDisabled = true;
            $scope.nextPersonDisabled = true;
        }
        if ($scope.selectedPage === 0) {
            $scope.previousPersonDisabled = true;
        }
        var max;
        var start = ($scope.selectedPage * 5);
        if ($scope.selectedPage === (maxPages - 1)) {
            max = ($scope.persons.length - start);
            $scope.nextPersonDisabled = true;
        } else {
            max = 5;
            $scope.nextPersonDisabled = false;
        }
        for (var i = 0; i < max; i++) {
            $scope.visiblePersons[i] = $scope.persons[(i + start)].valueOf();
        }
    };

    // Function for showing next person page
    $scope.nextPersonPage = function () {
        $scope.previousPersonDisabled = false;
        $scope.selectedPage++;
        $scope.visiblePersons = [];
        var maxPages = Math.ceil(($scope.persons.length / 5));
        var start = ($scope.selectedPage * 5);
        var max;
        if ($scope.selectedPage === (maxPages - 1)) {
            max = ($scope.persons.length - start);
            $scope.nextPersonDisabled = true;
            $scope.nextPersonDisabled = true;
        } else {
            max = 5;
            $scope.nextPersonDisabled = false;
        }
        for (var i = 0; i < max; i++) {
            $scope.visiblePersons[i] = $scope.persons[(i + start)].valueOf();
        }
    };

    // Function for showing previous person page
    $scope.previousPersonPage = function () {
        $scope.nextPersonDisabled = false;
        $scope.selectedPage--;
        $scope.visiblePersons = [];
        if ($scope.selectedPage === 0) {
            $scope.previousPersonDisabled = true;
        }
        else {
            $scope.previousPersonDisabled = false;
        }
        var start = (($scope.selectedPage) * 5);
        for (var i = 4; i >= 0; i--) {
            $scope.visiblePersons[i] = $scope.persons[(i + start)].valueOf();
        }
    };



    /*
     // this gets person data when init is ready
     $scope.$on('initReady', function (event) {
     $http({
     method: 'GET',
     url: 'http://localhost:8080/microcrm/webresources/microCRM_handler/listpersons',
     headers: {'Content-Type': 'application/json'}
     }).success(function (data) {
     $scope.persons = data;
     console.log("data fect success");
     }).error(function () {
     console.log("Cannot get persons data");
     });
     });
     */
});